# Data flow in the chair

Here we sketch the way that data should take to move through the department. A respective scheme can be seen below.

![Data flow](./pictures/data_flow.png)

The data flow is structured around the [folder structure](folder_structure.md) that is mandatory for all co-workers.

* **Storing data**

  You find the details under [folder structure](folder_structure.md) and [storage and backup](storage_backup.md).

* **Designing experiments**

  Make your notes to design an experiment in plain text formats if possible, see the [general remarks](general_remarks.md) for more information on this. You can for example use e.g. LaTeX or Markdown (like this file here) files. Handwritten notes on paper which are afterwards digitalized and put into the respective project folder are regarded as plain text by definition, too. Avoid proprietary formats like xlsx, docx or the like.

* **Measuring**

  Data from measuring devices should be stored in a plain text format like csv or txt. Avoid saving files in binary or proprietary formats. Most programs will offer suitable saving options. The measurements should then be stored in the **unprocessed_data** folder in your project. 

* **Analyzing data**

  Analyzing data and dissemination are the steps in this workflow where proprietary formats can most probably not completely be avoided. Try to use open and plain text alternatives to Excel like e.g. the Python ecosystem. This will also enhance the reproduceability of your analysis with respect to others users as e.g. Excel spreadsheets can be very complicated for others to read.

  Many journals offer primarily templates for Microsoft Office and you are free to use them. The same holds true for poster templates or talks at conferences. Here very often Powerpoint will be used but keep in mind that the graphics can e.g. be made with open source software.

  Avoid Origin, our senior scientists do not use it at all.

* **Sharing data**

  Where you share your data depends on the purpose. If it is for a publication use repositories like Zenodo or similar ones. Your data should then be in a shape that externals should be able to understand what you did. If you share data within a projekt group then tools like Sciebo are a good choice. If you want to share your data within the chair then everything should be stored on the central share anyway.

* **Archiving data**

  Archiving in our workflow means the long time storage of data in a [FAIR](https://www.forschungsdaten.org/index.php/FAIR_data_principles) way. This should be data where you and your PI have decided that they have some kind of value for the community. You can store these data on platforms like RWTH's [Coscine](https://about.coscine.de/) or if it is code you can use RWTH's [Gitlab](https://git.rwth-aachen.de/) instance or even [Github](https://github.com/) itself.

## Tools

The chair will try to offer software tools that will help you with this workflow. These software tools are tailored around this workflow like e.g. a tools that creates an empty standard folder structure or a tool that creates a metadata json file for you.

Apart from that all steps in the data workflow are designed to also work manually in case that you like to do it on your own, prefer your tools or have to do it manually in case everything else fails.