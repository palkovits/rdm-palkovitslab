# Metadata

Every sample has to be acompanied with a metadata file. This file should be put in the **unprocessed_data** folder. Creating a file for each sample is mandatory as those files are used to harvest information about the samples in the group.

The file should be a **json-file** with the following fields:

```json
{
    "sample_id": "STP-CAT-001",
    "manufacturer": "Stefan Palkovits",
    "uuid4": "35adc66a-f211-460e-84f2-f866e67b390c",
    "sample_description": "Pt on Graphene",
    "sample_procedures": "Incipient wetness impregnation",
    "parent": "parent sample",
    "child": "child sample",
    "analytics":
        {
            "XRD": ["./XRD/STP-CAT-001_XRD_01.txt", "./XRD/STP-CAT-001_XRD_02.txt"],
            "Sorption": "./Sorption/STP-CAT-001_Sorp.txt"
        },
    "evaluation":
        {
            "XRD": "../processed_data/XRD/STP-CAT-001_XRD.ipynb",
            "Sorption": "../processed_data/Sorption/STP-CAT-001_Sorp.ipynb"
        },
    "dissemination":
        {
            "paper_01": "../dissemination/paper_01/manuscript.ipynb"
        }
}
```

For the unique identifier you should use the UUID4 algorithm. You can e.g. use the uuid4() function from the uuid Python package. The UUID can also e.g. be used to create QR codes to mark your samples. Under [general remarks](general_remarks.md) you can find more information about the naming conventions for samples and files.
