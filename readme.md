# Readme

In this repository a Research Data Workflow for the Chair of Heterogeneous Catalysis and Chemical Technology is layed out.

## Table of contents

- [x] [General Remarks](general_remarks.md)
- [x] [Data flow](data_flow.md)
- [x] [Folder structure](folder_structure.md)
- [x] [Metadata](metadata.md)
- [x] [Storage and Backup](storage_backup.md)
- [x] [Tools](tools.md)
- []

For styling tipps of Markdown files see the [markdown cheat sheet](markdown-cheat-sheet.md).

In case of question write an email to [Stefan Palkovits](stefan.palkovits@itmc.rwth-aachen.de).