// Created by Stefan Palkovits <stefan.palkovits@itmc.rwth-aachen.de>
// Small package to create a RDM folder structure
// Created initially with the help of Google Bard

package main

import (
	"fmt"
	"os"
)

func main() {

	// Ask for input
	var folder string
	fmt.Print("Please give a name for your project folder: ")
	fmt.Scanln(&folder)

	// Eingabe ausgeben
	fmt.Println("The folder will be called: ", folder)

	// Folder structure
	foldernames := []string{folder,
		folder + "/01_project_related",
		folder + "/02_unprocessed_data",
		folder + "/03_processed_data",
		folder + "/04_dissemination",
		folder + "/05_miscellaneous"}

	// Creating the folder structure
	for _, foldername := range foldernames {
		err := os.MkdirAll(foldername, 0755)
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	fmt.Println("Folder structure created")
}
