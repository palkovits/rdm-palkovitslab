import os

folder_structure = {
    "folder_01": "01_project_related",
    "folder_02": "02_unprocessed_data",
    "folder_03": "03_processed_data",
    "folder_04": "04_dissemination",
    "folder_05": "05_miscellaneous"
}

project = input("Enter projet name:")

for key, folder in folder_structure.items():
    path = './'+project+'/'+folder
    os.makedirs(path)