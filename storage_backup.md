# Storage

You should store your project folders in a central space, the **PalkovitsLab share** on the institutes servers. You are allowed keep copies of the project folder in a place that suits you. It is advantageous when you do not store (too much or too big) files in your user profile directly because it will slow down your log on process without any advantages. Storage places can be e.g.:

* Institute internal storage
  
  It is mandatory that you store the project folders on the server disk space provided by the institute in the **PalkovitsLab share**. Every coworker can use it and it should directly be mapped when you log on to your computer. The biggest advantage is that your data directly goes into the institutes backup. If necessary you can synchronize the folder additionally with tools like e.g. [Sciebo](https://hochschulcloud.nrw). This share will be used to harvest metadata.

* A local file storage
  
  You are allowed to store copies of the project folder on a local hard drive if it is more comfortable for you. You can even use a portable drive like a USB thumb drive for that purpose but take care of a proper backup of your data especially in this case. If you are maybe working abroad take take that all changed and added files end up in the **PalkovitsLab share**.

If you have some kind of sensitive data you can additionally encrypt your data e.g. with tools like [Cryptomator](https://cryptomator.org).

# Backup

**Keep in mind that synchronization is not a backup of your data! Errors will be spread over multiple devices.**

You as a coworker are responsible for a suitable backup strategy. Possible solutions are:

* Usage of the Institute internal storage

  Like already stated above you are obliged to use the institutes storage. This should directly end up in the tape backup of the university. This should be the safest way without many efforts.

* Local storage to ITMC server
  
  In case that you store your projects on the local harddrive of your computer like some subfolder of drive C: or on an external drive make frequent copies to the ITMC server to end up in the university backup. You should make incremental backups in order to not flood the server space. If it is more comfortable you can additionally synchronize the folder to your online storage. For security reasons [Sciebo](https://hochschulcloud.nrw) would be the best option.