# General remarks

The research data management strategy for the group for Heterogeneous Catalysis and Chemical Technology makes some assumptions and is guided by some more general aspects.

These are:

1. **One central storage space for a project**
2. **Only a few naming conventions**
3. **Keep raw data and add proper metadata**
4. **Use open tools and formats as much as possible**
5. **Freedom of choice for personal tools**

You will find out more information about these points in the next passages.

## 1. One central storage space for a project

For every project a central storage space should exist in the form of a local folder on the institutes server storage on the **PalkovitsLab share**. You are also free to keep a personal copy on a physical medium like a flash drive. You can find more information on the folder system in the [folder structure](folder_structure.md) document. More information on storage can be found in the [storage and backup](storage_backup.md) document.

## 2. **Only a few naming conventions**

Name the files that you produce in a proper way. The filename should contain the date when you created the document, a small topic and an abbreviation for the coworker who produced it. An example might look like this:

*220101_GeneralRemarksOnRDM_STP.csv*

Sample names are for the coworkers conveniece but still mandatory. Sample names should be in the following form:

*STP-CAT-001*

* The first three characters are an abbreviation of the coworker. In most cases this will be the first character of your first name, the first character of a second name or the second character of the first name and the first character of your last name.
* Then three characters. These characters give brief information about the nature of the sample. 

    * CAT (alyst)
    * EXP (eriment, can be used for a catalytic result) 
    * SOL (vent)
    * SUP (port)
    * ELE (ctrolyte)
    * LIG (and)

* Lastly a three digit continous sample number.

When analytics is done on the sample the respective technique can be added to the sample name. If you do the same technique multiple times add two digits to the end.

*STP-CAT-001_XRD*

*STP-CAT-002_XRD_01* and *STP-CAT-002_XRD_02*

Abbreviations for the techniques are the following:

HPLC, GC, MS, ICP, CHN, Sorp, TPR, TPD, TPO, Pulse, IR, Drifts, Raman, UVVIS, UPS, XRD, XPS, EXAFS, XAFS, XRF, PDF, NMR, EPR, SEM, TEM, ECCV, ECEIS, TG, RI, BP, Others

In the required [metadata](metadata.md) file this will be acompanied with unique identifier (uuid4).

## 3. **Keep raw data and add proper metadata**

You should leave the raw data that you receive e.g. from a machine untouched. If possible choose the data file that you receive in the form of a plain text format. If not try to add a plain text export to the original file with the same name. Please take a look at [metadata](metadata.md) for further information. This might look like this:

* STP-CAT-002_UVVIS.bin
* STP-CAT-002_UVVIS.csv

## 4. **Use open tools and formats as much as possible**

You are obliged to use open software tools and data formats as long as possible. Please check the [tools](tools.md) document for more in depth information. Unfortunately this also means that you should avoid software like Excel, Origin or Word because the files they produce are proprietary. Files prepared by those are not meant to end up in the **unprocessed_data** folder. Use plain text based formats like LaTeX or Markdown. For calculations prefer tools like Python or similar things. You can find more inspiration under [tools](tools.md).

## 5. **Freedom of choice for personal tools**

Apart from the rough guidelines above you are allowed to choose the tools that suit you best. If you like you can also use tools that are to some extend already established in the institute like FurthRMind by [FurtRresearch](https://www.furthr-research.com).



