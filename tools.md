# Possible tools for Research Data Management @PalkovitsLab

There is a multitude of tools that can be used for the workflow that is proposed in this document. You should use open source tools and plain text formats as long and as often as possible. See also the [general remarks](general_remarks.md) for more information on this. Avoid proprietary programs and formats if possible. At some point this might not be avoided, especially when it comes to dissemination like papers or talks.

Do not use Origin, our senior scientists do not use it at all.

Here is an incomplete list of things that can be used:

## Making lab notes

* Handwritten and digitalized notes

* Markdown files

* LaTeX files

* FurthrMind

* openBIS

## Analyizing and ploting data

* Python/Jupyter

* Excel

## Drawing chemistry

* ChemDraw

* LaTeX

* If your chemistry allows it use machine readable formats like SMILES, INCHI or SELFIES to write your formulae.

## Sharing data

* Zenodo
  
* Sciebo

## Archiving data

* [Coscine](https://coscine.rwth-aachen.de/login/?redirect=%2F) and the [Coscine Python SDK](https://git.rwth-aachen.de/coscine/community-features/coscine-python-sdk)

* [Gitlab](https://git.rwth-aachen.de/)

* [Github](https://github.com/) 

## Tools to avoid as long as possible

* Microsoft Office (Word, Excel, Powerpoint). Very often those tools are still indispensable for publications and talks.

## Tools without any support in the group

* Origin
* Matlab

This list is meant as inspiration. For suggestions for additional tools contact your seniors.