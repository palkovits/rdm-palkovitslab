#!/usr/bin/env python3
import os
import shutil
from pathlib import Path
from time import sleep

from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler

import timeit
import logging

import pandas as pd
import PyPDF2
from matplotlib import pyplot as plt


def generate_plot(save_path, x_coordinate, y_coordinate, ending):
    fig = plt.figure()
    ax = fig.subplots()
    ax.plot(x_coordinate, y_coordinate)
    fig.savefig(save_path.with_name(save_path.name + f"_{ending}.png"))
    fig.clf()


def parse_float(text):
    try:
        float(text)
        return float(text.replace(",", ".")) if "," in text else float(text)
    except ValueError:
        return text


def parse_columns(lines):
    return list(zip(*(list(map(parse_float, line.split())) for line in lines)))


def parse_columns_of_table(lines, start_keyword, start_offset, end_keyword):
    start = lines.index(start_keyword) + start_offset
    end = lines.index(end_keyword) - 1
    table_columns = list(line.split() for line in lines[start:end])
    areas, concentrations, names = [], [], []
    for list_content in table_columns:
        area, conc, name = list_content[4], list_content[7], list_content[10]
        areas.append(area)
        concentrations.append(conc)
        names.append(name)
    return names, areas, concentrations


def parse_columns_between(lines, start_keyword, start_offset, end_keyword):
    start = lines.index(start_keyword) + start_offset
    end = lines.index(end_keyword) - 1
    return parse_columns(lines[start:end])


def get_xy_data(file_path):
    with file_path.open(encoding="ascii") as lines:
        next(lines)  # Skip header line.
        return parse_columns(lines)


def running_xrd(file_path):
    if not "unprocessed_data" in str(file_path) or str(file_path.suffix) not in [".png", ".xlsx"]:
        angles, intensities = get_xy_data(file_path)
        generate_plot(file_path, angles, intensities, "One")


def get_xy_data_chromatogramm(file_path):
    with file_path.open(encoding="ascii") as file:
        lines = list(file)

    if "LabSolutions" in lines[1]:
        ri_start_keyword, ri_end_keyword = "[LC Chromatogram(Detector A-Ch1)]\n", "[LC Status Trace(Pump A Pressure)]\n"
        uv_start_keyword, uv_end_keyword = "[PDA Multi Chromatogram(Ch1)]\n", "[PDA Multi Chromatogram(Ch2)]\n"
    else:
        ri_start_keyword, ri_end_keyword = "[LC Chromatogram(Detector B-Ch1)]\n", "[LC Status Trace(Pump A Pressure)]\n"
        uv_start_keyword, uv_end_keyword = "[LC Chromatogram(Detector A-Ch1)]\n", "[LC Chromatogram(Detector B-Ch1)]\n"

    ri_times, ri_intensities = parse_columns_between(lines, ri_start_keyword, 8, ri_end_keyword)
    uv_times, uv_intensities = parse_columns_between(lines, uv_start_keyword, 11, uv_end_keyword)
    return ri_times, ri_intensities, uv_times, uv_intensities


def get_mass_data_chromatogramm(file_path):
    with file_path.open(encoding="ascii") as file:
        lines = list(file)
    if "LabSolutions" in lines[1]:
        start_keyword, end_keyword = "[Peak Table(Detector A)]\n", "[Peak Table(PDA-Ch1)]\n"
    else:
        start_keyword, end_keyword = "[Peak Table(Detector B-Ch1)]\n", "[Peak Table(PDA-Ch1)]\n"
    return parse_columns_of_table(lines, start_keyword, 3, end_keyword)


def generate_csv(file_path, names, areas, concentrations):
    pd.DataFrame({"Name: ": names, "Area: ": areas, "Konz: ": concentrations}).T.to_csv(f"{file_path}_short.txt")


def running_hplc(file_path):
    if not "processed_data" in str(file_path) or str(file_path.suffix) not in [".png", ".xlsx"] and not "short" in file_path.stem:
        (ri_times, ri_intensities, uv_times, uv_intensities) = get_xy_data_chromatogramm(file_path)
        generate_plot(file_path, ri_times, ri_intensities, "RI")
        generate_plot(file_path, uv_times, uv_intensities, "UV")
        generate_csv(file_path, *get_mass_data_chromatogramm(file_path))

def pdf_file_to_list(file_path):
    return [page.extract_text() for page in PyPDF2.PdfReader(file_path).pages]

def pdf_file_to_txt_file(file_path):
    with open(f"{file_path}.txt", "w") as file:
        pdf_content = pdf_file_to_list(file_path)
        for item in pdf_content:
            file.write(item)


class CopyFileEventHandler(LoggingEventHandler):
    def on_created(self, event):
        super().on_created(event)
        sleep(0.2)
        source_path = Path(event.src_path)
        if not event.is_directory:
            if "unprocessed_data" in str(source_path):
                try:
                    shutil.copy2(event.src_path,
                                 f"{os.path.dirname(event.src_path).replace('unprocessed_data', 'processed_data')}\\generated_{os.path.basename(event.src_path)}")
                except PermissionError:
                    with open("debug.log", "a", encoding="utf-8") as logfile:
                        logfile.write(f"Zugangsfehler: {source_path}\n")
            if not "unprocessed_data" in str(source_path):
                sleep(0.5)
                if "hplc" in str(source_path.stem).lower() and "short" not in str(source_path.stem).lower():
                    running_hplc(source_path)
                elif "exported" in str(source_path.stem).lower() and str(source_path.suffix) not in [".png", ".xlsx"]:
                    running_xrd(source_path)
                elif ".pdf" in str(source_path.suffix).lower():
                    pdf_file_to_txt_file(source_path)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S', handlers=[logging.FileHandler("debug.log"), logging.StreamHandler()])

    #path = "S:\Palkovits\intern"
    path = "E:\PythonRDM@PalkovitsLab"
    event_handler = CopyFileEventHandler()
    observer = Observer()
    start = timeit.default_timer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    end = timeit.default_timer()
    try:
        pass
    except KeyboardInterrupt:
        observer.stop()
    observer.join()