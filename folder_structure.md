# The folder structure for projects

Every project is based on a 5-fold folder structure like one can see in the graphic below.

![Folder structure](./pictures/folder_structure.drawio.png)

Each folder should use the following naming convention and contain the respective information. Make sure that everything is readable and processable in all (programming-) languages. E.g. make sure that you omit umlauts, special characters and blanks:

* **project_name**:
  
  The top folder of each project should be named after the project it stands for. This can be e.g. a short title of a Ph.D. thesis or a catalyst class.

* **01_project_related**
  
  The *project_related* folder should contain relevant information for your project. This can for example be the project proposal if you are on a funded project or on a stipend. You can also put relevant literature into a subfolder of this one.

* **02_unprocessed_data**
  
  The *unprocessed_data* folder should contain any raw data originating from your project. Please create a subfolder per sample that contains the data about the analytics and the catalysis:

  * sample_name
    * XRD_data
    * sorption_data
    * catalysis_data
  
  The data in this folder can also go partly into an online repository, so keep it in proper shape.

* **03_processed_data**

  The *processed_data* folder should contain any further analysis that you do on the data. Do not make copy of the raw data into this folder but link the analysis you make to the respective original files. Please do not copy and paste data between programs.

* **04_dissemination**

  This is the folder were your manuscripts go. This can either be paper drafts but also reports or similar written content. Please make one folder per manuscript. If the written content goes through revisions like in the case of a paper you should make subfolders for each revision:

  * manuscript_01
    * submission
    * revision_01
  * report
 
  Of course you should find a suitable titles instead of *manuscript*.

* **05_miscellaneous**

  Into this folder you can put all the content that has no suitable home in the other folders.

Details about storage and possible backup strategies can be found in the [storage and backup](storage_backup.md) document.

